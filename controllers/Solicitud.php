<?php
require_once "../models/solicitudModel.php";
require_once "../models/AsignadoModel.php";
require_once "../models/RechazadoModel.php";

$option = $_REQUEST['op'];
$objSolicitud = new SolicitudModel();
$objAsignado = new AsignadoModel();
$objRechazado = new RechazadoModel();

switch ($option) {
    case 'listsolicitudes':       
        listar_solicitudes($objSolicitud);
        break;
    case 'listaAsignados':       
        listar_solicitudes_asignado($objAsignado);
        break;
    case 'listaRechazados':       
        listar_solicitudes_rechazado($objRechazado);
        break;
    case 'versolicitud':
        ver_solicitudes($objSolicitud);
        break;
    case 'solicitud':
        solicitud($objSolicitud);
        break;
    case 'asignar':
        asignar($objSolicitud,$objAsignado);
        break;
    case 'rechazar':
        rechazar($objSolicitud,$objRechazado);
        break;    
    default:
        # code...
        break;
}

function listar_solicitudes($objSolicitud) {   
    $arrResponse = array('status' => false, 'data' => "");
    $arrSolicitud = $objSolicitud->getSolicitud();
  
    if (!empty($arrSolicitud)) {

        for ($i = 0; $i < count($arrSolicitud); $i++) {
            $id_solicitud = $arrSolicitud[$i]->id_solicitud;
            $options = '<a href="' . BASE_URL . 'views/solicitud/asignar_cita.php?p=' . $id_solicitud . '" class="btn 
                btn-outline-primary btn-sm" title="Asignar Cita"><i class="fa-regular fa-calendar-check"></i></a>
                <button class="btn btn-outline-danger btn-sm" title="Descartar Solicitud" onclick="fntDescSolicitud(' . $id_solicitud . ')">
                   <i class="fa-solid fa-ban"></i></button>';
            $arrSolicitud[$i]->options = $options;
        }

        $arrResponse['status'] = true;
        $arrResponse['data'] = $arrSolicitud;
    }
    echo json_encode($arrResponse);
  
}
function listar_solicitudes_asignado($objAsignado) {   
    $arrResponse = array('status' => false, 'data' => "");
    $arrSolicitud = $objAsignado->getAll();
  
    if (!empty($arrSolicitud)) {

        for ($i = 0; $i < count($arrSolicitud); $i++) {
            $id_solicitud = $arrSolicitud[$i]->id_solicitud;
            $options = '<a href="' . BASE_URL . 'views/solicitud/asignar_cita.php?p=' . $id_solicitud . '" class="btn 
                btn-outline-primary btn-sm" title="Asignar Cita"><i class="fa-regular fa-calendar-check"></i></a>
                <button class="btn btn-outline-danger btn-sm" title="Descartar Solicitud" onclick="fntDescSolicitud(' . $id_solicitud . ')">
                   <i class="fa-solid fa-ban"></i></button>';
            $arrSolicitud[$i]->options = $options;
        }

        $arrResponse['status'] = true;
        $arrResponse['data'] = $arrSolicitud;
    }
    echo json_encode($arrResponse);
  
}
function listar_solicitudes_rechazado($objRechazado) {   
    $arrResponse = array('status' => false, 'data' => "");
    $arrSolicitud = $objRechazado->getAll();
  
    if (!empty($arrSolicitud)) {

        for ($i = 0; $i < count($arrSolicitud); $i++) {
            $id_solicitud = $arrSolicitud[$i]->id_solicitud;
            $options = '<a href="' . BASE_URL . 'views/solicitud/asignar_cita.php?p=' . $id_solicitud . '" class="btn 
                btn-outline-primary btn-sm" title="Asignar Cita"><i class="fa-regular fa-calendar-check"></i></a>
                <button class="btn btn-outline-danger btn-sm" title="Descartar Solicitud" onclick="fntDescSolicitud(' . $id_solicitud . ')">
                   <i class="fa-solid fa-ban"></i></button>';
            $arrSolicitud[$i]->options = $options;
        }

        $arrResponse['status'] = true;
        $arrResponse['data'] = $arrSolicitud;
    }
    echo json_encode($arrResponse);
  
}

function ver_solicitudes($objSolicitud) {  
    if ($_POST) {
        $id_solicitud = intval($_POST['id_solicitud']);
        $arrSolicitud = $objSolicitud->getRegistro_id($id_solicitud);
       
        if (empty($arrSolicitud)) {
            $arrResponse = array('status' => false, 'msg' => 'Datos no encontrados');
        } else {
            $arrResponse = array('status' => true, 'msg' => 'Datos encontrados', 'data' => $arrSolicitud);
        }
        echo json_encode($arrResponse);
    }   
}

function solicitud($objSolicitud) {
    if ($_POST) {
        if (
            empty($_POST['txtIdentificacion']) || empty($_POST['txtNombre']) ||  empty($_POST['txtApellido1']) ||
            empty($_POST['txtApellido2']) || empty($_POST['txtEmail'])|| empty($_POST['txtExamen']) ||empty($_POST['txtObservacion']))
         {
            $arrResponse = array('status' => false, 'msg' => 'Error de datos');
        } else {
            $intIdentificacion = trim($_POST['txtIdentificacion']);
            $strNombre = ucwords(trim($_POST['txtNombre']));
            $strApellido1 = ucwords(trim($_POST['txtApellido1']));
            $strApellido2 = ucwords(trim($_POST['txtApellido2']));
            $strEmail = strtolower(trim($_POST['txtEmail']));
            $strExamen = ucwords(trim($_POST['txtExamen']));
            $strObservacion = ucwords(trim($_POST['txtObservacion']));

            $arrSolicitud = $objSolicitud->insertSolicitud(
                $intIdentificacion,
                $strNombre,
                $strApellido1,
                $strApellido2,
                $strEmail,
                $strExamen,
                $strObservacion
            );

            if ($arrSolicitud->id > 0) {

                $arrResponse = array('status' => true, 'msg' => 'Datos Guardados Correctamente');
            } else {
                $arrResponse = array('status' => false, 'msg' => 'Error al Guardar los Datos');
            }
        }

        echo json_encode($arrResponse);
    }
}

function asignar($objSolicitud,$objAsignado) {  
    $r = new stdClass();
    $r->id_solicitud=trim($_POST['id_solicitud']);
    $r->identificacion=trim($_POST['identificacion']);
    $r->nombre=trim($_POST['nombre']);
    $r->apellido1=trim($_POST['apellido1']);
    $r->apellido2=trim($_POST['apellido2']);
    $r->email=trim($_POST['email']);
    $r->tipo_examen=trim($_POST['tipo_examen']);
    $r->solicitud=trim($_POST['solicitud']);
    $r->rechazo_motivo=trim($_POST['rechazo_motivo']);
    $r->observacion=trim($_POST['observacion']);
    $r->fecha_solicitud = trim($_POST['fecha_solicitud_dia']).' '.trim($_POST['fecha_solicitud_hora']);
    $r->estado=2;//trim($_POST['estado']);
   
    $muestra1 = $presentarse_con = $horario = $muestra2 = $muestra3 = $muestra4 = '';
    if(isset($_POST['traer_muestras1'])){$muestra1 = $_POST['traer_muestras1'];}
    if(isset($_POST['traer_muestras2'])){$muestra2 = $_POST['traer_muestras2'];}
    if(isset($_POST['traer_muestras3'])){$muestra3 = $_POST['traer_muestras3'];}
    if(isset($_POST['traer_muestras4'])){$muestra4 = $_POST['traer_muestras4'];}
    if(isset($_POST['horario'])){$horario = $_POST['horario'];}
    if(isset($_POST['presentarse_con'])){$presentarse_con = $_POST['presentarse_con'];}
    
    $r->traer_muestras=$muestra1.$muestra2.$muestra3.$muestra4;
    $r->horario=$horario;
    $r->presentarse_con=$presentarse_con;

    if(empty($r->identificacion))
    {
       $arrResponse = array('status' => false, 'msg' => 'Error de datos');
    }else{
         /* GUARDAMOS REGISTRO EN TB ASIGNADO */
         $arrSolicitud = $objSolicitud->update_estado($r);
         $arrAsignado = $objAsignado->save($r); 
         /* php Mailer */
           mailTipoSolicitud($r);
         /* php Mailer */         
        
         if (intval($arrAsignado->id_solicitud) > 0) {
            $arrResponse = array('status' => true, 'msg' => 'Datos Guardados Correctamente');
        } else {
            $arrResponse = array('status' => false, 'msg' => 'Error al Guardar los Datos');
        }
    }  
  
    echo json_encode($arrResponse);
}
function rechazar($objSolicitud,$objRechazado) {  
    $id_solicitud = trim($_POST['id_solicitud']);
    $registro = $objSolicitud->getRegistro_id($id_solicitud);

    $r = new stdClass();
    $r->id_solicitud=trim($_POST['id_solicitud']);
    $r->identificacion=trim($_POST['identificacion']);
    $r->nombre=trim($_POST['nombre']);
    $r->apellido1=trim($_POST['apellido1']);
    $r->apellido2=trim($_POST['apellido2']);
    $r->email='palominoeduardo01@gmail.com';//trim($_POST['email']);
    $r->tipo_examen=trim($_POST['tipo_examen']);
    $r->solicitud=trim($_POST['solicitud']);
    $r->rechazo_motivo=trim($_POST['rechazo_motivo']);
    $r->observacion=trim($_POST['observacion']);
    $r->fecha_solicitud = trim($_POST['fecha_solicitud_dia']).' '.trim($_POST['fecha_solicitud_hora']);
    $r->estado=3;//trim($_POST['estado']);   
    
    $muestra1 = $presentarse_con = $horario = $muestra2 = $muestra3 = $muestra4 = '';
    if(isset($_POST['traer_muestras1'])){$muestra1 = $_POST['traer_muestras1'];}
    if(isset($_POST['traer_muestras2'])){$muestra2 = $_POST['traer_muestras2'];}
    if(isset($_POST['traer_muestras3'])){$muestra3 = $_POST['traer_muestras3'];}
    if(isset($_POST['traer_muestras4'])){$muestra4 = $_POST['traer_muestras4'];}
    if(isset($_POST['horario'])){$horario = $_POST['horario'];}
    if(isset($_POST['presentarse_con'])){$presentarse_con = $_POST['presentarse_con'];}
    
    $r->traer_muestras=$muestra1.$muestra2.$muestra3.$muestra4;
    $r->horario=$horario;
    $r->presentarse_con=$presentarse_con;
   
    if(empty($r->identificacion))
    {
       $arrResponse = array('status' => false, 'msg' => 'Error de datos');
    }else{
         /* GUARDAMOS REGISTRO EN TB rechazado */
         $arrSolicitud = $objSolicitud->update_estado($r);
         $arrAsignado = $objRechazado->save($r);         
         /* var_dump($arrAsignado);
         exit; */
          /* php Mailer */
          $r->tipo_examen="RECHAZADO";
          mailTipoSolicitud($r);
          /* php Mailer */ 


         if (intval($arrAsignado->id_solicitud) > 0) {
            $arrResponse = array('status' => true, 'msg' => 'Datos Guardados Correctamente');
        } else {
            $arrResponse = array('status' => false, 'msg' => 'Error al Guardar los Datos');
        }
    }   

    echo json_encode($arrResponse);
}

function mailTipoSolicitud($r){
    $objEspecialidad = 1;
    switch ($r->tipo_examen) {
      case 'LABORATORIO':
          $objEspecialidad = 1;
          break;
      case 'RX':
          $objEspecialidad = 2;
          break;
      case 'US':
          $objEspecialidad = 3;
      case 'ULTRASONIDO':
          $objEspecialidad = 4;
          break;            
      default:
          $objEspecialidad = 5;
          break;
    }
    $objMensaje = new stdClass();
    $objMensaje->emailDestino=$r->email;
    $objMensaje->asunto='Mensaje de sistema '.$r->tipo_examen;
    $html = plantillaHtml($objEspecialidad,$r);
    $objMensaje->html= $html;          
    phpMailer($objMensaje);
}

function phpMailer($objMensaje) {
    $emailDestinatario = $objMensaje->emailDestino;//'palominoeduardo01@gmail.com';
    //--------------------- MAIL -----------------------------
    $nameOrigen = 'globalelectricalservicescr';
    $emailOrigen = 'eduardo@globalelectricalservicescr.com';//'coreo@empresa.ccom';
    $passwordOrigen = 'Palomino1234*';
    
     require '../libraries/miphpmailer/PHPMailer.php';
     require '../libraries/miphpmailer/SMTP.php';
     require '../libraries/miphpmailer/Exception.php';

    $mail = new PHPMailer\PHPMailer\PHPMailer();
    $mail->SMTPDebug = 1;
    $mail->isSMTP();
 
    $mail->Host = 'mail.globalelectricalservicescr.com';    
    $mail->SMTPAuth = true;
    $mail->Username = $emailOrigen;
    $mail->Password = $passwordOrigen;     
    $mail->SMTPSecure = 'tls';
    $mail->Port = 587;//465;
    $mail->SMTPDebug = 0;
    $mail->setFrom($emailOrigen, $nameOrigen);
    $mail->addAddress($emailDestinatario);     
    $mail->isHTML(true);
    $mail->Subject =$objMensaje->asunto;//;
    $mail->Body = $objMensaje->html;
    $dt = $mail->send();       
    if (!$mail->send()) {
      return 'Mailer Error: ' . $mail->ErrorInfo;
     } else {
       return  'El mensaje fue enviado existosamente al correo.';
     }
    //--------------------- END MAIL -------------------------
}
function plantillaHtml($objEspecialidad,$r) {
    
   $htmlRespuestaMamografias = '<p><strong>Estimado usuario(a)</strong></p>

   <p><strong>ID</strong> : '.$r->identificacion.'&nbsp;&nbsp;&nbsp; <strong>NOMBRE&nbsp; </strong>: '.$r->nombre.' '.$r->apellido1.' '.$r->apellido2.'&nbsp;&nbsp;</p>
   
   <p><strong>FECHA HORA CITA</strong>: '.$r->fecha_solicitud.'</p>   
  <strong>PRESENTARSE 15 MINUTOS ANTES DE LAS CITAS </strong></p>
   <ul>
       <li>NO USAR TALCO NI DESODORANTE</li>
       <li>NO TRAER NI&Ntilde;OS</li>
       <li>NO USAR OBJETOS EN LAS OREJAS, MANOS NI CUELLOS.</li>
       <li>POR FAVOR VENIR RECIEN BA&Ntilde;ADA</li>
   </ul>   
   <p><span style="font-size:24px"><strong>EL DIA DE LA CITA TRAER EL FORMULARIO LLENO POR AMBOS LADOS </strong></span></p>';
 
   $htmlRespuestaAutomaticaLaboratorio ='<p><strong>Estimado usuario(a)</strong></p>

   <p><strong>ID</strong> : '.$r->identificacion.'&nbsp;&nbsp;&nbsp; <strong>NOMBRE&nbsp; </strong>: '.$r->nombre.' '.$r->apellido1.' '.$r->apellido2.'&nbsp;&nbsp;</p>
   
   <p><strong>FECHA HORA CITA</strong>: '.$r->fecha_solicitud.'</p> <p><strong><span style="font-size:18px">Indicaciones Generales: </span></strong></p>
   <p>1. Presentarse en el horario indicado en la VENTANILLA del Laboratorio con sus documentos al d&iacute;a y en buen estado.<br />
   2. El ayuno incluye no consumir chicles, golosinas, ni bebidas azucaradas.<br />
   3. Antes de tomar la muestra de sangre debe de abstenerse 24 horas de consumir alcohol y realizar ejercicio intenso, y no puede fumar.<br />
   4. Presentarse en el siguiente horario:<br />
   '.$r->horario.'<br />
   5. Debe presentarse con:<br />
   '.$r->presentarse_con.'<br />
   6. Debe traer las siquientes muestras rotuladas manualmente:<br />
   '.$r->traer_muestras.'<br />
   </p>';

   $htmlIndicacionesUltrasonido = '<p><strong>Estimado usuario(a)</strong></p>

   <p><strong>ID</strong> : '.$r->identificacion.'&nbsp;&nbsp;&nbsp; <strong>NOMBRE&nbsp; </strong>: '.$r->nombre.' '.$r->apellido1.' '.$r->apellido2.'&nbsp;&nbsp;</p>
   
   <p><strong>FECHA HORA CITA</strong>: '.$r->fecha_solicitud.'</p> <p style="margin-left:0; margin-right:0">&quot;Estimado usuario(a)&quot;</p>
   <p style="margin-left:0; margin-right:0">El &Aacute;rea de Salud Goicoechea 2, le informa que su cita fue asignada para el d&iacute;'.$r->fecha_solicitud.'. Debe presentarse en la Cl. Jim&eacute;nez N&uacute;&ntilde;ez en ____________.</p>
   <p style="margin-left:0; margin-right:0">&nbsp;</p>   
   <p style="margin-left:0; margin-right:0">&nbsp;</p>   
   <p style="margin-left:0; margin-right:0">Indicaciones para los estudios de radiolog&iacute;a.</p>
   <p style="margin-left:0; margin-right:0">&nbsp;</p>   
   <p style="margin-left:0; margin-right:0">Vias Urinarias, pelvico, ginecologico, abdomen inferior o pelvico: tomar</p>
   <p style="margin-left:0; margin-right:0">2 litros de agua dos horas antes y no orinar.</p>
   <p style="margin-left:0; margin-right:0">&nbsp;</p>
   <p style="margin-left:0; margin-right:0">Abdomen completo o superior: Ayuno de 8 horas</p>
   <p style="margin-left:0; margin-right:0">&nbsp;</p>   
   <p style="margin-left:0; margin-right:0">Mamas: Sin desodorante</p>   
   <p style="margin-left:0; margin-right:0">&nbsp;</p>   
   <p style="margin-left:0; margin-right:0">Doppler arterias renales: Dia anterior de la cita dieta blanda sin harinas y el dia de la cita ayuno completo.</p>';

   $htmlRayosX ='<p><strong>Estimado usuario(a)</strong></p>

   <p><strong>ID</strong> : '.$r->identificacion.'&nbsp;&nbsp;&nbsp; <strong>NOMBRE&nbsp; </strong>: '.$r->nombre.' '.$r->apellido1.' '.$r->apellido2.'&nbsp;&nbsp;</p>
   
   <p><strong>FECHA HORA CITA</strong>: '.$r->fecha_solicitud.'</p> Rayos X';

    // $Laboratorio 1
    // $RayosX 2
    // $Mamografias 3
    // $Ultrasonido 4

   $html='';
   $objEspecialidad = (int)$objEspecialidad;
    switch ($objEspecialidad) {        
        case 1:
            $html = $htmlRespuestaAutomaticaLaboratorio;
            break;
        case 2:
            $html = $htmlRayosX;
            break; 
        case 3:
            $html = $htmlRespuestaMamografias;
            break;
        case 4:
            $html = $htmlIndicacionesUltrasonido;
            break;
        case 5:
            $html = 'Su solicitud fue rechazaa debido a '.$r->rechazo_motivo;
            break; 
        default:
            # code...
            break;
    }
    return $html;
}
