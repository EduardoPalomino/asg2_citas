function fntDescSolicitud(id){

/*     swal({
        title: "Desea descartar la solicitud recibida?",
        text: "",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
      .then((willDelete) => {
        if (willDelete) {
          $.post(base_url+"controllers/Solicitud.php?op=rechazar", {id_solicitud: id}, function(repuesta){
           // $("#cargaexterna").html(repuesta);
          });        
          swal("Solicitud Descartada!", {
            icon: "success",
          });

        } else {
          swal("Se ha cancelado la acción de descartar solicitud!");
        }
      }); */

      location.href = base_url+'views/solicitud/rechazar_cita.php?p='+id;
}
async function fntAsignar() {
  //const id_solicitud = $("#id_solicitud").val();
  const identificacion = $("#identificacion").val();
  const nombre = $("#nombre").val();
  const apellido1 = $("#apellido1").val();
  const apellido2 = $("#apellido2").val();
  const email = $("#email").val();
  const tipo_examen = $("#tipo_examen").val();
  //const solicitud = $("#solicitud").val();
  //const rechazo_motivo = $("#rechazo_motivo").val();
  const observacion = $("#observacion").val();
  //const fecha_solicitud = $("#fecha_solicitud").val();
  //const estado = $("#estado").val();
  //alert(" asignando ...");
  if(identificacion == "" || nombre == "" || apellido1 == "" || apellido2 == "" || email == "" 
  ||tipo_examen ==""){
      alert("Todos los campos son obligatorios");
      return;
  }
  if(tipo_examen =="LABORATORIO"){
    if($('input[name="horario"]').is(':checked')){
   
    }else{
      alert("Seleccione Horario de Presentación");
      return;
    }
    if($('input[name="presentarse_con"]').is(':checked')){   
    }else{
      alert("Seleccione como se presentará el paciente");
      return;
    }
    if($('.muestras').is(':checked')){   
    }else{
      alert("Seleccione muestras");
      return;
    }
  }
  try{

      const data = new FormData(frmNuevaCita);
      let resp = await fetch(base_url+"controllers/Solicitud.php?op=asignar",{
          method: 'POST',
          mode: 'cors',
          cache: 'no-cache',
          body: data
      }); 
      json = await resp.json();
      if(json.status){
          swal("Guardar",json.msg,"success");
          //frmNuevaCita.reset();          
          location.href = base_url+'views/solicitud/';
      }else{
          swal("Guardar",json.msg,"error");
      }
     
      
  }catch(err){

      console.log("Ocurrió un error "+err);

  }
}  
async function fntRechazar() {
 
  const identificacion = $("#identificacion").val();
  const nombre = $("#nombre").val();
  const apellido1 = $("#apellido1").val();
  const apellido2 = $("#apellido2").val();
  const email = $("#email").val();
  const tipo_examen = $("#tipo_examen").val();
  const observacion = $("#observacion").val(); 
  if(identificacion == "" || nombre == "" || apellido1 == "" || apellido2 == "" || email == "" 
  ||tipo_examen ==""){
      alert("Todos los campos son obligatorios");
      return;
  }
  try{
      const data = new FormData(frmNuevaCitaRechazo);
      let resp = await fetch(base_url+"controllers/Solicitud.php?op=rechazar",{
          method: 'POST',
          mode: 'cors',
          cache: 'no-cache',
          body: data
      }); 
      json = await resp.json();
      if(json.status){
          swal("Guardar",json.msg,"success");
          //frmNuevaCita.reset();          
          location.href = base_url+'views/solicitud/';
      }else{
          swal("Guardar",json.msg,"error");
      }
     
      
  }catch(err){

      console.log("Ocurrió un error "+err);

  }
}  
