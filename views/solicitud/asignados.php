<?php require "../template/header.php";
?>

<main class="container">
    <h1 class="text-center">Lista de Asignados</h1>
    <a href=" <?= BASE_URL ?>/views/solicitud/">Lista Solicitudes</a>
    <table id="tbl_solicitud" class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Identificación</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Primer Apellido</th>
                    <th scope="col">Segundo Apellido</th>
                    <th scope="col">Email</th>
                    <th scope="col">Tipo de Examen</th>
                    <th scope="col">Solicitud</th>
                    <th scope="col">Fecha de Solicitud</th>
                    
                </tr>
            </thead>
            <tbody = id="tblBodySolicitudes_asignados">
            
                
            </tbody>
        </table>


</main>

<?php require "../template/footer.php";
?>

<script src="../template/js/functions-solicitud.js"></script>
<script>
    let id_persona = "<?= $_GET['p'] ?>";
    fntMostrar(id_persona);
</script>