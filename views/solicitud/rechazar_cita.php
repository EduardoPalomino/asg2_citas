<?php require "../template/header.php";
?>

<main class="container">
    <h1 class="text-center">Rechazar  Cita</h1>
    <a href=" <?= BASE_URL ?>/views/solicitud/">Lista Solicitudes</a>
    <br>
    <br>
    <form id="frmNuevaCitaRechazo">
        <input type="hidden" name="id_solicitud" id="id_solicitud" required="">
        <input type="hidden" name="estado" id="estado">
        <div class="mb-3">
            <label for="txtFecha" class="form-label">Fecha de Cita</label>
           <input type="date"   min="01-01-1997" max="31-12-2030"  class="form-control" id="fecha_solicitud_dia" name="fecha_solicitud_dia" placeholder="Fecha para la Cita" required>
       

            
        </div>
       
        </div>

        <div class="mb-3">
            <label for="txtHora" class="form-label">Hora de Cita</label>
            <input type="time" class="form-control" value="17:17:46" id="fecha_solicitud_hora" name="fecha_solicitud_hora" placeholder="Hora para la Cita" required>
        </div>

        <div class="mb-3">
            <label for="txtIdentificacion" class="form-label">Identificación</label>
            <input type="text" class="form-control" id="identificacion" name="identificacion" placeholder="No. de Cédula" required>

        </div>
        <div class="mb-3">
            <label for="nombre" class="form-label">Nombre</label>
            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required>
        </div>
        <div class="mb-3">
            <label for="txtApellido1" class="form-label">Primer Apellido</label>
            <input type="text" class="form-control" id="apellido1" name="apellido1" placeholder="Primer Apellido" required>
        </div>
        <div class="mb-3">
            <label for="txtApellido2" class="form-label">Segundo Apellido</label>
            <input type="text" class="form-control" id="apellido2" name="apellido2" placeholder="Segundo Apellido" required>
        </div>
        <div class="mb-3">
            <label for="txtEmail" class="form-label">Correo electrónico</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Correo electrónico" required>
        </div>

        <div class="mb-3">
            <label for="txtExamen" class="form-label">Tipo de Examen</label>
            <input type="text" class="form-control" id="tipo_examen" name="tipo_examen" placeholder="Examen por Realizar" required>
        </div>
        <div class="mb-3">
            <label for="txtExamen" class="form-label">Solicitud</label>
            <input type="text" class="form-control" id="solicitud" name="solicitud" placeholder="Solicitud" required>
        </div>
        <div class="mb-3">
            <label for="txtExamen" class="form-label">Motivo Rechazo</label>
            <input type="text" class="form-control" id="rechazo_motivo" name="rechazo_motivo" placeholder="Motivo Rechazo" required>
        </div>
        <div class="mb-3">
            <label for="txtObservacion" class="form-label">Observaciones</label>
            <input type="text" class="form-control" id="observacion" name="observacion" placeholder="observacion">
        </div>

        <button type="button" onclick="fntRechazar()" class="btn btn-success"><i class="fa-solid fa-share-from-square"></i> Guardar / Enviar Email</button>
    </form>


</main>

<?php require "../template/footer.php";
?>

<script src="../template/js/functions-solicitud.js"></script>
<script>
    let id_persona = "<?= $_GET['p'] ?>";
    fntMostrar(id_persona);
</script>