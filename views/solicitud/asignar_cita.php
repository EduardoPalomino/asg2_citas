<?php require "../template/header.php";
?>

<main class="container">
    <h1 class="text-center">Asignar Nueva Cita</h1>
    <a href=" <?= BASE_URL ?>/views/solicitud/">Lista Solicitudes</a>
    <br>
    <br>
    <form id="frmNuevaCita">
        <input type="hidden" name="id_solicitud" id="id_solicitud" required="">
        <input type="hidden" name="estado" id="estado">
        <div class="mb-3">
            <label for="txtFecha" class="form-label">Fecha de Cita</label>
            <input type="date" min="01-01-1997" max="31-12-2030" class="form-control" id="fecha_solicitud_dia" name="fecha_solicitud_dia" placeholder="Fecha para la Cita" required>



        </div>

        </div>

        <div class="mb-3">
            <label for="txtHora" class="form-label">Hora de Cita</label>
            <input type="time" class="form-control" value="17:17:46" id="fecha_solicitud_hora" name="fecha_solicitud_hora" placeholder="Hora para la Cita" required>
        </div>

        <div class="mb-3">
            <label for="txtIdentificacion" class="form-label">Identificación</label>
            <input type="text" class="form-control" id="identificacion" name="identificacion" placeholder="No. de Cédula" required>

        </div>
        <div class="mb-3">
            <label for="nombre" class="form-label">Nombre</label>
            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required>
        </div>
        <div class="mb-3">
            <label for="txtApellido1" class="form-label">Primer Apellido</label>
            <input type="text" class="form-control" id="apellido1" name="apellido1" placeholder="Primer Apellido" required>
        </div>
        <div class="mb-3">
            <label for="txtApellido2" class="form-label">Segundo Apellido</label>
            <input type="text" class="form-control" id="apellido2" name="apellido2" placeholder="Segundo Apellido" required>
        </div>
        <div class="mb-3">
            <label for="txtEmail" class="form-label">Correo electrónico</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="Correo electrónico" required>
        </div>

        <div class="mb-3">
            <label for="txtExamen" class="form-label">Tipo de Examen</label>
            <input type="text" class="form-control" id="tipo_examen" name="tipo_examen" placeholder="Examen por Realizar" required>
        </div>
        <div class="mb-3">
            <label for="txtExamen" class="form-label">Solicitud</label>
            <input type="text" class="form-control" id="solicitud" name="solicitud" placeholder="Solicitud" required>
        </div>
        <div class="mb-3" style="display:none;">
            <label for="txtExamen" class="form-label">Motivo Rechazo</label>
            <input type="text" class="form-control" id="rechazo_motivo" name="rechazo_motivo" placeholder="Motivo Rechazo" required>
        </div>
        <div class="mb-3" style="display:none;">
            <label for="txtObservacion" class="form-label">Observaciones</label>
            <input type="text" class="form-control" id="observacion" name="observacion" placeholder="observacion">
        </div>
        <div class="mb-3 labot">
            <label class="form-label">Presentarse en el siguiente horario:</label>
            
            <div class="form-check">
            <input class="form-check-input" value="  - 6:00 am - 7:30 am" type="radio" name="horario" id="horario1">
                <label class="form-check-label">
                   6:00 am - 7:30 am
                </label>
            </div>
            <div class="form-check">
            <input class="form-check-input" value="  - 7:00 am en Puerta de Emergencias"   type="radio" name="horario" id="horario2">
                <label class="form-check-label">
                7:00 am en Puerta de Emergencias
                </label>
            </div>

        </div>
        <div class="mb-3 labot">
            <label class="form-label">Debe presentarse con:</label>
  
            <div class="form-check">
            <input class="form-check-input" type="radio" value="  - Ayuno de 12 horas"  name="presentarse_con" id="presentarse_con1">
                <label class="form-check-label">
                Ayuno de 12 horas
                </label>
            </div>
            <div class="form-check">
            <input class="form-check-input" type="radio"  value="  - Sin Ayuno"  name="presentarse_con" id="presentarse_con2">
                <label class="form-check-label">
                Sin Ayuno
                </label>
            </div>
            <div class="form-check">
            <input class="form-check-input"  value="  - 8 Horas de Ayuno"    type="radio" name="presentarse_con" id="presentarse_con3">
                <label class="form-check-label">
                8 Horas de Ayuno
                </label>
            </div>

        </div>
        <div class="mb-3 labot">
            <label class="form-label">Debe traer las siquientes muestras rotuladas manualmente:</label>
            <div class="form-check">
            <input class="muestras form-check-input"  value="  (X) Primera orina de la mañana <br />"   type="checkbox" name="traer_muestras1" id="traer_muestras1">
                <label class="form-check-label">
                Primera orina de la mañana
                </label>
            </div>          
            <div class="form-check">
            <input class="muestras form-check-input"   value="  (X) Heces <br />"  type="checkbox" name="traer_muestras2" id="traer_muestras2">
                <label class="form-check-label">
                Heces
                </label>
            </div>          
            <div class="form-check">
            <input class="muestras form-check-input"   value="  (X) Orina de 24 horas <br />"    type="checkbox" name="traer_muestras3" id="traer_muestras3">
                <label class="form-check-label">
                Orina de 24 horas
                </label>
            </div>          
            <div class="form-check">
            <input class="muestras form-check-input"     value="  (X) Sin muestras <br />"   type="checkbox" name="traer_muestras4" id="traer_muestras4">
                <label class="form-check-label">
                Sin muestras
                </label>
            </div>          

        </div>
        <button type="button" onclick="fntAsignar()" class="btn btn-success"><i class="fa-solid fa-share-from-square"></i> Guardar / Enviar Email</button>
    </form>


</main>

<?php require "../template/footer.php";
?>

<script src="../template/js/functions-solicitud.js"></script>
<script>
    let id_persona = "<?= $_GET['p'] ?>";
    fntMostrar(id_persona);
</script>