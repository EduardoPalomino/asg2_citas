<?php require "../template/header.php";
?>

<main class="container">
    <h1 class="text-center">Solicitar Cita ASG2</h1>
    <a href=" <?= BASE_URL ?>views/solicitud/">Lista Solicitudes</a>
    <br>
    <br>
    <form id="frmSolicitud">
        <div class="mb-3">
            <label for="txtIdentificacion" class="form-label">Identificación</label>
            <input type="text" class="form-control" id="txtIdentificacion" name="txtIdentificacion" placeholder="No. de Cédula" required>

        </div>

        <div class="mb-3">
            <label for="txtNombre" class="form-label">Nombre</label>
            <input type="text" class="form-control" id="txtNombre" name="txtNombre" placeholder="Nombre" required>
        </div>

        <div class="mb-3">
            <label for="txtApellido1" class="form-label">Primer Apellido</label>
            <input type="text" class="form-control" id="txtApellido1" name="txtApellido1" placeholder="Primer Apellido" required>
        </div>
        <div class="mb-3">
            <label for="txtApellido2" class="form-label">Segundo Apellido</label>
            <input type="text" class="form-control" id="txtApellido2" name="txtApellido2" placeholder="Segundo Apellido" required>
        </div>
        <div class="mb-3">
            <label for="txtEmail" class="form-label">Correo electrónico</label>
            <input type="email" class="form-control" id="txtEmail" name="txtEmail" placeholder="Correo electrónico" required>
        </div>

        <div class="mb-3">
            <label for="txtExamen" class="form-label">Tipo de Examen</label>
            <input type="text" class="form-control" id="txtExamen" name="txtExamen" placeholder="Examen por Realizar" required>
        </div>
        <div class="mb-3">
            <label for="txtObservacion" class="form-label">Observaciones</label>
            <input type="text" class="form-control" id="txtObservacion" name="txtObservacion" placeholder="Detalle su Solicitud">
        </div>

        <button type="submit" class="btn btn-info"><i class="fa-solid fa-share-from-square"></i> Enviar</button>
    </form>


</main>

<?php require "../template/footer.php";
?>

<script src="../template/js/functions-solicitud.js"></script>