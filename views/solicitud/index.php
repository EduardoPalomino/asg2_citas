<?php require "../template/header.php";
 ?>

    <main class="container">
        <h1 class="text-center">Listado Solicitudes</h1>
        <a href="solicitud.php" class="btn btn-success">Solicitar Cita <i class="fa-solid fa-user-plus"></i></a>
        <table id="tbl_solicitud" class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Identificación</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Primer Apellido</th>
                    <th scope="col">Segundo Apellido</th>
                    <th scope="col">Email</th>
                    <th scope="col">Tipo de Examen</th>
                    <th scope="col">Solicitud</th>
                    <th scope="col">Boleta</th>
                    <th scope="col">Fecha de Solicitud</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
            <tbody = id="tblBodySolicitudes">
            
                
            </tbody>
        </table>
    </main>

    <?php require "../template/footer.php";
 ?>

 <script src="../template/js/functions-solicitud.js"></script>

