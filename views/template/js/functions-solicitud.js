async function getSolicitud() {
    try {

        let resp = await fetch(base_url+"controllers/Solicitud.php?op=listsolicitudes");
       
        json = await resp.json();
        if (json.status) {
            let data = json.data;
            data.forEach((item) => {
                let newtr = document.createElement("tr");
                newtr.id = "row_" + item.id_solicitud;
                newtr.innerHTML = `<tr>
                <th scope="row">${item.id_solicitud}</th>
                <td>${item.identificacion}</td>
                <td>${item.nombre}</td>
                <td>${item.apellido1}</td>
                <td>${item.apellido2}</td>
                <td>${item.email}</td>
                <td>${item.tipo_examen}</td>
                <td>${item.solicitud}</td>
                <td><a href="../../../boletas/boleta.jpg" target="_blank"> <img src="../../../boletas/boleta.jpg" alt="Boleta" width="30" height="15"> </a>
               </td>
                <td>${item.fecha_solicitud}</td>
                
                <td>${item.options}</td>`

                document.querySelector("#tblBodySolicitudes").appendChild(newtr);
            });


        }

    } catch (err) {
       
    }
}
async function getSolicitud_rechazado() {
    try {

        let resp = await fetch(base_url+"controllers/Solicitud.php?op=listaRechazados");
       
        json = await resp.json();
        if (json.status) {
            let data = json.data;
            data.forEach((item) => {
                let newtr = document.createElement("tr");
                newtr.id = "row_" + item.id_solicitud;
                newtr.innerHTML = `<tr>
                <th scope="row">${item.id_solicitud}</th>
                <td>${item.identificacion}</td>
                <td>${item.nombre}</td>
                <td>${item.apellido1}</td>
                <td>${item.apellido2}</td>
                <td>${item.email}</td>
                <td>${item.tipo_examen}</td>
                <td>${item.solicitud}</td>
                <td>${item.fecha_solicitud}</td>`;

                document.querySelector("#tblBodySolicitudes_rechazado").appendChild(newtr);
            });


        }

    } catch (err) {
       
    }
}
async function getSolicitud_asignado() {
    try {

        let resp = await fetch(base_url+"controllers/Solicitud.php?op=listaAsignados");
       
        json = await resp.json();
        if (json.status) {
            let data = json.data;
            data.forEach((item) => {
                let newtr = document.createElement("tr");
                newtr.id = "row_" + item.id_solicitud;
                newtr.innerHTML = `<tr>
                <th scope="row">${item.id_solicitud}</th>
                <td>${item.identificacion}</td>
                <td>${item.nombre}</td>
                <td>${item.apellido1}</td>
                <td>${item.apellido2}</td>
                <td>${item.email}</td>
                <td>${item.tipo_examen}</td>
                <td>${item.solicitud}</td>
                <td>${item.fecha_solicitud}</td>`;

                document.querySelector("#tblBodySolicitudes_asignados").appendChild(newtr);
            });


        }

    } catch (err) {
       
    }
}
async function fntGuardar(){

    let strIdentificaion = document.querySelector("#txtIdentificacion").value;
    let strNombre = document.querySelector("#txtNombre").value;
    let strApellido1 = document.querySelector("#txtApellido1").value;
    let strApellido2 = document.querySelector("#txtApellido2").value;
    let strCorreo = document.querySelector("#txtEmail").value;
    let strExamen = document.querySelector("#txtExamen").value;
    let strObservaciones = document.querySelector("#txtObservacion").value;

    if(strIdentificaion == "" || strNombre == "" || strApellido1 == "" || strApellido2 == "" || strCorreo == "" 
    ||strExamen =="" ||strObservaciones == "" ){
        alert("Todos los campos son obligatorios");
        return;
    }
    try{

        const data = new FormData(frmRSolicitud);
        let resp = await fetch(base_url+"controllers/Solicitud.php?op=solicitud",{
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            body: data

        }); 
        json = await resp.json();
        if(json.status){
            swal("Guardar",json.msg,"success");
            frmRegistro.reset();
        }else{
            swal("Guardar",json.msg,"error");
        }
       
        
    }catch(err){

        console.log("Ocurrió un error "+err);

    }
}
async function fntMostrar(id){
    const formData = new FormData();
    formData.append('id_solicitud',id);
    

    try {

        let resp=await fetch(base_url+"controllers/Solicitud.php?op=versolicitud",{
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            body: formData
        });

        json = await resp.json();
        if(json.status){
            document.querySelector("#id_solicitud").value =json.data.id_solicitud;
            document.querySelector("#identificacion").value =json.data.identificacion;
            document.querySelector("#nombre").value =json.data.nombre;
            document.querySelector("#apellido1").value =json.data.apellido1;
            document.querySelector("#apellido2").value =json.data.apellido2;
            document.querySelector("#email").value =json.data.email;
            document.querySelector("#tipo_examen").value =json.data.tipo_examen;
            document.querySelector("#solicitud").value =json.data.solicitud;
            const dataFecha = json.data.fecha_solicitud.split(" ");
            document.querySelector("#fecha_solicitud_dia").value = dataFecha[0];//2023-06-27 16:09:40
            document.querySelector("#fecha_solicitud_hora").value = dataFecha[1];//2023-06-27 16:09:40
            
            /* document.querySelector("#txtId").value =json.data.id_solicitud;
            document.querySelector("#txtIdentificacion").value = json.data.identificacion;
            document.querySelector("#txtApellido1").value = json.data.apellido1;
            document.querySelector("#txtApellido2").value = json.data.apellido2;
            document.querySelector("#txtEmail").value = json.data.email;
            document.querySelector("#txtExamen").value = json.data.tipo_examen; */

            let tipo_examen = document.querySelector("#tipo_examen").value;
            console.log("fff valor yipo "+tipo_examen);
          
            if(tipo_examen =="LABORATORIO"){   
              $(".labot").css("display", "block");
              console.log(1);
            }else{
                console.log(2);
            }



        }else{
            window.location = base_url+'views/solicitud';
        }

       
        
    } catch (err) {

        console.log("Ocurrió un error: "+err);
        
    }
}
if(document.querySelector("#tblBodySolicitudes_asignados")){
   
    getSolicitud_asignado();
}
if(document.querySelector("#tblBodySolicitudes_rechazado")){
   
    getSolicitud_rechazado();
}

if(document.querySelector("#tblBodySolicitudes")){
    getSolicitud();
   
}

if(document.querySelector("#frmSolicitud")){
    let frmRSolicitud = document.querySelector("#frmSolicitud");
    frmRSolicitud.onsubmit =function(e){
        e.preventDefault();//Previene que se recargue al darle clic al botón Enviar o Guardar
        fntGuardar();
    }


}

//----- CREAR CITA---
if(document.querySelector("#frmNuevaCita")){
    let frmRSolicitud = document.querySelector("#frmNuevaCita");
    frmRSolicitud.onsubmit =function(e){
        e.preventDefault();//Previene que se recargue al darle clic al botón Enviar o Guardar
        fntGuardar();
    }
}





