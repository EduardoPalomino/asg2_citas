async function getSolicitud() {
    try {

        let resp = await fetch(base_url+"controllers/Solicitud.php?op=listsolicitudes");
        json = await resp.json();
        if (json.status) {
            let data = json.data;
            data.forEach((item) => {
                let newtr = document.createElement("tr");
                newtr.id = "row_" + item.id_solicitud;
                newtr.innerHTML = `<tr>
                <th scope="row">${item.id_solicitud}</th>
                <td>${item.identificacion}</td>
                <td>${item.nombre}</td>
                <td>${item.apellido1}</td>
                <td>${item.apellido2}</td>
                <td>${item.email}</td>
                <td>${item.tipo_examen}</td>
                <td>${item.solicitud}</td>
                <td>${item.fecha_solicitud}</td>
                
                <td>${item.options}</td>`

                document.querySelector("#tblBodySolicitudes").appendChild(newtr);
            });


        }

    } catch (err) {
       
    }
}
if(document.querySelector("#tblBodySolicitudes")){

    getSolicitud();

}

if(document.querySelector("#frmSolicitud")){
    let frmRSolicitud = document.querySelector("#frmSolicitud");
    frmRSolicitud.onsubmit =function(e){
        e.preventDefault();//Previene que se recargue al darle clic al botón Enviar o Guardar
        fntGuardar();
    }

    async function fntGuardar(){

        let strIdentificaion = document.querySelector("#txtIdentificacion").value;
        let strNombre = document.querySelector("#txtNombre").value;
        let strApellido1 = document.querySelector("#txtApellido1").value;
        let strApellido2 = document.querySelector("#txtApellido2").value;
        let strCorreo = document.querySelector("#txtEmail").value;
        let strExamen = document.querySelector("#txtExamen").value;
        let strObservaciones = document.querySelector("#txtObservacion").value;

        if(strIdentificaion == "" || strNombre == "" || strApellido1 == "" || strApellido2 == "" || strCorreo == "" 
        ||strExamen =="" ||strObservaciones == "" ){
            alert("Todos los campos son obligatorios");
            return;
        }
        try{

            const data = new FormData(frmRSolicitud);
            let resp = await fetch(base_url+"controllers/Solicitud.php?op=solicitud",{
                method: 'POST',
                mode: 'cors',
                cache: 'no-cache',
                body: data

            }); 
            json = await resp.json();
            if(json.status){
				swal("Guardar",json.msg,"success");
				frmRegistro.reset();
			}else{
				swal("Guardar",json.msg,"error");
			}
           
            
        }catch(err){

            console.log("Ocurrió un error "+err);

        }
    }
}

async function fntMostrar(id){
    const formData = new FormData();
    formData.append('id_solicitud',id);

    try {

        let resp=await fetch(base_url+"controllers/Solicitud.php?op=versolicitud",{
            method: 'POST',
            mode: 'cors',
            cache: 'no-cache',
            body: formData
        });

        json = await resp.json();
        if(json.status){

            document.querySelector("#txtId").value =json.data.id_solicitud;
            document.querySelector("#txtIdentificacion").value = json.data.identificacion;
            document.querySelector("#txtApellido1").value = json.data.apellido1;
            document.querySelector("#txtApellido2").value = json.data.apellido2;
            document.querySelector("#txtEmail").value = json.data.email;
            document.querySelector("#txtExamen").value = json.data.tipo_examen;

        }else{
            window.location = base_url+'views/solicitud';
        }

       
        
    } catch (err) {

        console.log("Ocurrió un error: "+err);
        
    }

}