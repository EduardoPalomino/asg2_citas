<?php 
    require_once "../libraries/conexion.php";
    class SolicitudModel{
        private $conexion;
        function __construct(){

            $this->conexion = new Conexion();

            $this->conexion= $this->conexion->conect();
            
        }
    

    public function getSolicitud(){
        $arrRegistros = array();
       // $rs = $this->conexion->query("CALL select_solicitud()");
        $rs = $this->conexion->query("select * FROM solicitud_cita where estado='1' ORDER BY id_solicitud DESC");
        while($obj =$rs->fetch_object()){
            array_push($arrRegistros,$obj);

        }
        
        return $arrRegistros;
    }

    public function insertSolicitud(int $identificacion, string $nombre, string $apellido1, string $apellido2,
    string $email, string $examen, string $observacion){

        $sql = $this->conexion->query("CALL insertar_solicitud('{$identificacion}','{$nombre}','{$apellido1}','{$apellido2}',
        '{$email}','{$examen}','{$observacion}')");
        $sql = $sql->fetch_object();
        return $sql;
    }

    public function getRegistro(int $id_solicitud){

        $sql = $this->conexion->query("CALL select_solicitud('{$id_solicitud}')");
        $sql = $sql->fetch_object();
        return $sql;

    }

    public function getRegistro_id($id_solicitud){
        $sql = $this->conexion->query("select * FROM solicitud_cita where id_solicitud=".$id_solicitud);
        $sql = $sql->fetch_object();
        return $sql;
    }
    public function update_estado($r){
        $sql = $this->conexion->query("update solicitud_cita SET estado = '".$r->estado."' WHERE id_solicitud = ".$r->id_solicitud);
        $rs = $this->getRegistro_id($r->id_solicitud);
        return $sql;
    }
    
    

}

    
?>