<?php 
    require_once "../libraries/conexion.php";
    class AsignadoModel{
        private $conexion;
        function __construct(){
            $this->conexion = new Conexion();
            $this->conexion= $this->conexion->conect();            
        }
    

    public function getAll(){
        $arrRegistros = array();
        $rs = $this->conexion->query("select * FROM asignado ORDER BY id_solicitud DESC");
        while($obj =$rs->fetch_object()){
            array_push($arrRegistros,$obj);
        }        
        return $arrRegistros;
    }

     /*     
        public function getId($id){
        $sql = $this->conexion->query("select * from asignado where id_solicitud =".$id);
        $sql = $sql->fetch_object();
        return $sql;
    } */

    public function lastRow(){
        $sql = $this->conexion->query("select * FROM asignado ORDER BY id_solicitud DESC LIMIT 1");
        $sql = $sql->fetch_object();
        return $sql;
    }

    public function save($r){
        $sql = $this->conexion->query("insert into asignado 
        (id_solicitud,identificacion,nombre,apellido1,apellido2,email,tipo_examen,solicitud,rechazo_motivo,observacion,fecha_solicitud,estado) VALUES 
        (NULL,'".$r->identificacion."', '".$r->nombre."', '".$r->apellido1."', '".$r->apellido2."', '".$r->email."', '".$r->tipo_examen."', '".$r->solicitud."', '".$r->rechazo_motivo."', '".$r->observacion."','".$r->fecha_solicitud."', '".$r->estado."')");
        $lastRow = $this->lastRow();
        return $lastRow;
    }

    /*     
        public function update($registro){
        $sql = $this->conexion->query("");
        $sql = $sql->fetch_object();
        return $sql;
    } */

    /*     
        public function delete($id){
        $sql = $this->conexion->query("");
        $sql = $sql->fetch_object();
        return $sql;
    } */    

}    
?>